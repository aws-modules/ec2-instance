locals {
  tags = var.use_default_tags ? {} : {
    environment             = var.environment
    product                 = var.product
    use_case                = var.use_case
    Can_be_deleted          = true
    Created_using_terraform = true
  }
  name     = "${var.environment}-${var.product}-${var.use_case}-server"
  dns_name = "${var.environment}-${var.product}-${var.use_case}"
}
