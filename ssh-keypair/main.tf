resource "tls_private_key" "this" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

locals {
  key_name = "env:/${var.environment}/ssh-keys/${var.environment}-${var.product}-${var.use_case}-keypair.pem"
}

resource "aws_s3_object" "this" {
  count    = var.save_private_key ? 1 : 0
  key      = local.key_name
  bucket   = var.storage_bucket_name
  content  = tls_private_key.this.private_key_pem
  tags     = local.tags
  metadata = {}

}
resource "aws_key_pair" "this" {
  key_name   = "${var.environment}-${var.product}-${var.use_case}-keypair"
  public_key = tls_private_key.this.public_key_openssh
  tags       = local.tags
}

locals {
  tags = var.use_default_tags ? {} : {
    Environment             = var.environment
    Product                 = var.product
    Use_case                = var.use_case
    Can_be_deleted          = true
    Created_using_terraform = true
  }
}