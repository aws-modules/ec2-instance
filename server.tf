resource "aws_eip" "this" {
  count    = var.create_eip ? 1 : 0
  instance = module.ec2_instance.id
  vpc      = true
  tags     = local.tags
}

module "ssh-keypair" {
  source              = "./ssh-keypair"
  product             = var.product
  use_case            = var.use_case
  environment         = var.environment
  storage_bucket_name = var.ssh_key_bucket_name
  save_private_key    = var.save_private_key
  use_default_tags    = var.use_default_tags
}


module "ec2_instance" {
  source                      = "terraform-aws-modules/ec2-instance/aws"
  version                     = "4.0.0"
  name                        = local.name
  create_spot_instance        = var.create_spot_instance
  spot_block_duration_minutes = var.spot_block_duration_minutes
  tags                        = local.tags
  volume_tags                 = local.tags
  enable_volume_tags          = var.enable_volume_tags
  ami                         = var.ami
  associate_public_ip_address = var.associate_public_ip_address
  availability_zone           = var.availability_zone
  disable_api_termination     = var.disable_api_termination
  ebs_block_device            = var.ebs_block_device
  ebs_optimized               = var.ebs_optimized
  iam_instance_profile        = var.iam_instance_profile
  instance_type               = var.instance_type
  key_name                    = module.ssh-keypair.key_name
  monitoring                  = var.monitoring
  vpc_security_group_ids      = [aws_security_group.this.id]
  subnet_id                   = var.subnet_id
  root_block_device           = var.root_block_device
  user_data                   = var.user_data
}

resource "aws_security_group" "this" {
  name                   = "${local.name}-sg"
  vpc_id                 = var.vpc_id
  description            = "${local.name}-sg"
  revoke_rules_on_delete = true
  tags                   = local.tags
  dynamic "ingress" {
    for_each = var.ingress_rules
    content {
      protocol        = ingress.value["protocol"]
      cidr_blocks     = ingress.value["cidr_blocks"]
      from_port       = ingress.value["from_port"]
      to_port         = ingress.value["to_port"]
      security_groups = ingress.value["security_groups"]

    }
  }
  dynamic "egress" {
    for_each = var.egress_rules
    content {
      protocol        = egress.value["protocol"]
      cidr_blocks     = egress.value["cidr_blocks"]
      from_port       = egress.value["from_port"]
      to_port         = egress.value["to_port"]
      security_groups = egress.value["security_groups"]
    }
  }
}

resource "null_resource" "remote_exec" {
  count = var.remote_exec_code == null ? 0 : 1

  triggers = {
    remote_exec_code = var.remote_exec_code
  }

  connection {
    type        = "ssh"
    host        = var.remote_exec_using_public_ip ? var.create_eip ? aws_eip.this.0.public_ip : module.ec2_instance.public_ip : module.ec2_instance.private_ip
    private_key = module.ssh-keypair.private_key
    user        = var.ssh_user
  }

  provisioner "remote-exec" {
    inline = var.hold_remote_exec_for_sec == null ? [
      var.remote_exec_code
      ] : [
      "sleep ${var.hold_remote_exec_for_sec}",
      var.remote_exec_code
    ]
  }

  depends_on = [
    aws_security_group.this
  ]
}

data "aws_route53_zone" "this" {
  count        = var.is_dns_required ? 1 : 0
  name         = var.dns_zone_name
  private_zone = var.is_dns_zone_private
}

resource "aws_route53_record" "this" {
  count   = var.is_dns_required ? 1 : 0
  zone_id = data.aws_route53_zone.this[count.index].zone_id
  name    = "${local.dns_name}.${data.aws_route53_zone.this[count.index].name}"
  type    = "A"
  ttl     = var.private_record_ttl
  records = ["${module.ec2_instance.private_ip}"]
}
