output "keypair_details" {
  value     = module.ssh-keypair
  sensitive = true
}
output "instance_details" {
  value = module.ec2_instance
}
output "dns_details" {
  value = aws_route53_record.this
}
output "instance_sg_details" {
  value = aws_security_group.this
}